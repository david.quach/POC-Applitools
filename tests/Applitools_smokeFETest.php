<?php


use Applitools\RectangleSize;
use Applitools\Selenium\Eyes;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Applitools\Selenium\StitchMode;
use Facebook\WebDriver\WebDriver;

/**
 *
 * @category    selenium
 * @package     tests
 * @subpackage  smoke
 * @author      Martin Bedouret <martin@softwareadvice.com>
 */

/**
 * POC: Applitools

 *
 * @package     tests
 * @subpackage  smoke
 */
class Smoke_AppliFrontendTest extends PHPUnit_Extensions_Selenium2TestCase
{

    protected $_session;
    protected  $_driver;
    public function setUp()
    {

        $this->setBrowser('chrome');
        $this->setBrowserUrl("https://www.softwareadvice.com/");
        $this->_session = parent::prepareSession();

        // Using remote webdriver opens a totally new driver along with the driver opened by PHPUnit_Extensions_Selenium2TestCase
        // WE DO NOT WANT TO OPEN A NEW BROWSER
    //    $host = "http://localhost:4444/wd/hub";
    //    $this->_driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());

    }

    /**
     * <p>Preconditions:</p>
     */
    protected function assertPreConditions()
    {
    }


    /**
     * <p> POC for Applitools </p>
     *
     * @test
     *
     * @group smokeFrontendParallel11
     */
    public function test_HomePageTest()
    {
       //Uncomment below line to make the driver use the same session that selenium opens
       // THIS IS WHAT WE WANT TO USE
        $this->_driver = RemoteWebDriver::createBySessionID($this->_session->id(),$this->url());


        // Initialize the eyes SDK and set your private API key.
        $eyes = new Eyes();
        $eyes->setLogHandler(new \Applitools\FileLogger('tests/test.log', true, true));
        $eyes->setApiKey('QOvCOZ9dbOSV0eRBnxwcJAmCJRzCQoj47hy102w1zJz5I110');
        $appName = 'SA QA Eng';
        $testName = 'test_Home';

        $this->url("https://www.softwareadvice.com/");

        // $this->_driver->get("https://www.softwareadvice.com/");
         $eyes->open($this->_driver, $appName, $testName, new RectangleSize(800, 600));

        // Visual checkpoint.
         $eyes->checkWindow("Home");

        // End the test.
        $eyes->close();

    }




  }